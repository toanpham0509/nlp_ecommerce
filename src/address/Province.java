package address;

// TODO: Auto-generated Javadoc
/**
 * The Class Province.
 */
public class Province {

	/** The provinces. */
	private String[] provinces = { "ha noi", "ha giang", "cao bang", "bac can",
			"tuyen quang", "lao cai", "dien bien", "lai chau", "son la",
			"yen bai", "hoa binh", "thai nguyen", "lang son", "quang ninh",
			"bac giang", "phu tho", "vinh phuc", "bac ninh", "hai duong",
			"hai phong", "hung yen", "thai binh", "ha nam", "nam dinh",
			"ninh binh", "thanh hoa", "nghe an", "ha tinh", "quang binh",
			"quang tri", "thua thien hue", "da nang", "quang nam",
			"quang ngai", "binh dinh", "phu yen", "khanh hoa", "ninh thuan",
			"binh thuan", "kon tum", "gia lai", "dac lac", "dac nong",
			"lam dong", "binh phuoc", "tay ninh", "binh duong", "dong nai",
			"ba ria - vung tau", "ho chi minh", "long an", "tien giang",
			"ben tre", "tra vinh", "vinh long", "dong Thap", "an giang",
			"kien giang", "can tho", "hau giang", "soc trang", "bac lieu",
			"ca mau" };

	/**
	 * Gets the provinces.
	 * 
	 * @return the provinces
	 */
	public String[] getProvinces() {
		return provinces;
	}

	/**
	 * Sets the provinces.
	 * 
	 * @param provinces
	 *            the new provinces
	 */
	public void setProvinces(String[] provinces) {
		this.provinces = provinces;
	}

}
