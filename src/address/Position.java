package address;

// TODO: Auto-generated Javadoc
/**
 * The Class Position.
 */
public class Position {
	
	/** The index. */
	private int fromIndex;
	
	/** The value. */
	private String value;
	
	/** The length. */
	private int length;
	
	/** The end index. */
	private int endIndex;
	
	
	/** The new value. */
	private String newValue;
	
	
	/** 
	 * The type.
	 * 1: street 
	 * 2: ward 
	 * 3: district 
	 * 4: province
	 * */
	private int type;
	
	/**
	 * Instantiates a new position.
	 *
	 * @param value the value
	 * @param index the index
	 * @param type the type
	 */
	public Position(String value, int index, int type) {
		this.value = value;
		this.fromIndex = index;
		this.length = value.length();
		this.endIndex = this.fromIndex + this.length;
		this.type = type;
	}
	
	public Position(String value, int index, int type, String newValue) {
		this.value = value;
		this.fromIndex = index;
		this.length = value.length();
		this.endIndex = this.fromIndex + this.length;
		this.type = type;
		this.newValue = newValue;
	}
	
	/**
	 * Instantiates a new position.
	 */
	public Position() {
		
	}
	
	/**
	 * Show.
	 */
	public void show() {
		System.out.println("Type: " + this.type + 
				"\tFrom Index: " + this.fromIndex +
				"\tEnd Index: " + this.endIndex +
				"\tValue: " + this.value);
	}

	/**
	 * Gets the from index.
	 *
	 * @return the from index
	 */
	public int getFromIndex() {
		return fromIndex;
	}

	/**
	 * Sets the from index.
	 *
	 * @param fromIndex the new from index
	 */
	public void setFromIndex(int fromIndex) {
		this.fromIndex = fromIndex;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the length.
	 *
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * Sets the length.
	 *
	 * @param length the new length
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * Gets the end index.
	 *
	 * @return the end index
	 */
	public int getEndIndex() {
		return endIndex;
	}

	/**
	 * Sets the end index.
	 *
	 * @param endIndex the new end index
	 */
	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the newValue
	 */
	public String getNewValue() {
		return newValue;
	}

	/**
	 * @param newValue the newValue to set
	 */
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
}
