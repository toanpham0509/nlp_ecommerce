package address;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class AddressFilter.
 */
public class AddressFilter {
    
	/** The input. */
	private String input;
	
	/** The address. */
	private ArrayList<Position> address = new ArrayList<Position>();
	
	/** The province positions. */
	private ArrayList<Position> provincePositions = new ArrayList<Position>();
	
	/** The district positions. */
	private ArrayList<Position> districtPositions = new ArrayList<Position>();
	
	/** The street positions. */
	private ArrayList<Position> streetPositions = new ArrayList<Position>();
	
	/** The positions. */
	private ArrayList<Position> positions = new ArrayList<Position>();
	
	
	private ArrayList<WordPlace> wordFilters;
	
	/**
	 * Instantiates a new address validator.
	 *
	 * @param input the input
	 */
	public AddressFilter(String input) {
		this.setInput(input);
		this.init();
	}
	
	/**
	 * Instantiates a new address validator.
	 */
	public AddressFilter() {
		this.init();
	}
	
	/**
	 * Inits the.
	 */
	private void init() {
		this.address = new ArrayList<Position>();
		this.provincePositions = new ArrayList<Position>();
		this.districtPositions = new ArrayList<Position>();
		this.streetPositions = new ArrayList<Position>();
		this.positions = new ArrayList<Position>();
		
		this.wordFilters = new ArrayList<WordPlace>();
		wordFilters.add(new WordPlace("ng", "g"));
		wordFilters.add(new WordPlace("nh", "h"));
		wordFilters.add(new WordPlace("nh", "nk"));
		wordFilters.add(new WordPlace("ch", "c"));
		wordFilters.add(new WordPlace("ch", "ck"));
		wordFilters.add(new WordPlace("ch", "k"));
		wordFilters.add(new WordPlace("uyen", "uen"));
	}
	
    /**
     * Valid.
     *
     * @param input the input
     * @return the string
     */
    public String filter(String input) {
    	Province province = new Province();
    	District district = new District();
    	Street street = new Street();
    	
    	//get list provinces in string input
    	this.setProvincePositions(this.getPositions(input, province.getProvinces(), 4));
    	
    	//get list districts in string input
    	this.setDistrictPositions(this.getPositions(input, district.getDistricts(), 3));
    	
    	//get list streets in string input
    	this.setStreetPositions(this.getPositions(input, street.getStreets(), 1));
    	
    	this.getPositions().sort(new Comparator<Position>() {

			public int compare(Position o1, Position o2) {
				return o1.getFromIndex() - o2.getFromIndex();
			}
    	});
    	
    	int length = this.getPositions().size();
    	
    	System.out.println("\nKey word was found: ");
    	for(int i = 0; i < length; i++) {
    		this.getPositions().get(i).show();
    	}
    	
    	Position position;
    	Position address;
    	for(int i = 0; i < length; ) {
    		position = this.getPositions().get(i);
    		
    		if(position.getType() == 1) {
    			address = position;
    			
    			//find no home
    			for(int e = (address.getFromIndex() >= 10) ? address.getFromIndex() - 10 : 0; e < address.getFromIndex(); e++) {
    				if(Character.isDigit(input.charAt(e))) {
    					address.setFromIndex(e);
    					break;
    				}
    			}
    			
    			//Find ward, district, province
    			int j;
    			for(j = i + 1; j < length; j++) {
    				if(this.getPositions().get(j).getFromIndex() < address.getEndIndex() + 4) {
    					address.setEndIndex(this.getPositions().get(j).getEndIndex());
    				} else {
    					break;
    				}
    			}
    			
    			address.setValue(input.substring(address.getFromIndex(), address.getEndIndex()));
    			this.address.add(address);
    			i += (j - i);
    		} else {
    			i++;
    		}
    	}
    	
    	System.out.println("\nAddress was found: ");
    	for(Position add: this.getAddress()) {
    		add.show();
    		
    		input = input.replace(add.getValue(), "***");
    	}
    	
    	return input;
    }
     
    /**
     * Gets the district positions.
     *
     * @return the district positions
     */
    public ArrayList<Position> getDistrictPositions() {
		return districtPositions;
	}

	/**
	 * Sets the district positions.
	 *
	 * @param districtPositions the new district positions
	 */
	public void setDistrictPositions(ArrayList<Position> districtPositions) {
		this.districtPositions = districtPositions;
		this.positions.addAll(this.getDistrictPositions());
	}

	/**
	 * Gets the street positions.
	 *
	 * @return the street positions
	 */
	public ArrayList<Position> getStreetPositions() {
		return streetPositions;
	}

	/**
	 * Sets the street positions.
	 *
	 * @param streetPositions the new street positions
	 */
	public void setStreetPositions(ArrayList<Position> streetPositions) {
		this.streetPositions = streetPositions;
		this.positions.addAll(this.getStreetPositions());
	}

	/**
	 * Gets the positions.
	 *
	 * @param input the input
	 * @param params the params
	 * @param type the type
	 * @return the positions
	 */
    private ArrayList<Position> getPositions(String input, String[] params, int type) {
    	ArrayList<Position> positions = new ArrayList<Position>();
    	
    	int length = params.length;
    	for(int i = 0; i < length; i++) {
    		int fromIndex = -1; 
    		while(true) {
    			fromIndex = input.indexOf(params[i], fromIndex + 1);
    			if(fromIndex == -1) {
    				break;
    			}
    			positions.add(new Position(params[i], fromIndex, type));
    		}
    	}
    	
    	return positions;
    }

	/**
	 * Gets the province positions.
	 *
	 * @return the province positions
	 */
	public ArrayList<Position> getProvincePositions() {
		return provincePositions;
	}

	/**
	 * Sets the province positions.
	 *
	 * @param provincePositions the new province positions
	 */
	public void setProvincePositions(ArrayList<Position> provincePositions) {
		this.provincePositions = provincePositions;
		this.positions.addAll(this.getProvincePositions());
	}

	/**
	 * Gets the positions.
	 *
	 * @return the positions
	 */
	public ArrayList<Position> getPositions() {
		return this.positions;
	}

	/**
	 * Sets the positions.
	 *
	 * @param positions the new positions
	 */
	public void setPositions(ArrayList<Position> positions) {
		this.positions = positions;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public ArrayList<Position> getAddress() {
		return this.address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(ArrayList<Position> address) {
		this.address = address;
	}

	/**
	 * Gets the input.
	 *
	 * @return the input
	 */
	public String getInput() {
		return input;
	}

	/**
	 * Sets the input.
	 *
	 * @param input the input to set
	 */
	public void setInput(String input) {
		this.input = input;
	}

	/**
	 * Convert string serect.
	 *
	 * @return the string[]
	 */
	public ArrayList<String> convertStringFilter(String input) {
		ArrayList<Position> positions = new ArrayList<Position>();
		ArrayList<String> arrayString = new ArrayList<String>();
		arrayString.add(input);
		
		int length = wordFilters.size();
		int index = -1;
		for(int i = 0; i < length; i++) {
			while(true) {
				index = input.indexOf(wordFilters.get(i).getOldWord(), index + 1);
				if(index == -1) {
					break;
				} else {
					positions.add(new Position(wordFilters.get(i).getOldWord(), index, 1, wordFilters.get(i).getNewWord()));
				}
			}
		}
		
		if(positions.size() > 0) {
			switch(positions.size()) {
				case 1: {
					arrayString.add(input.replace(positions.get(0).getValue(), positions.get(0).getNewValue()));
					break;
				}
				default: {
					arrayString = this.replaceWord(arrayString, input, positions);
					break;
				}
			}
		}
		
//		for(String str : arrayString) {
//			System.out.println(str);
//		}
		
		return arrayString;
	}
	
	/**
	 * Replace word.
	 *
	 * @param arrayString the array string
	 * @param input the input
	 * @return the array list
	 */
	public ArrayList<String> replaceWord(ArrayList<String> arrayString, String input, ArrayList<Position> positions) {
		
		return arrayString;
	}
}
