package address;

// TODO: Auto-generated Javadoc
/**
 * The Class WordPlace.
 */
public class WordPlace {
	
	/** The old word. */
	private String oldWord;
	
	/** The new word. */
	private String newWord;
	
	/**
	 * Instantiates a new word place.
	 *
	 * @param oldWord the old word
	 * @param newWord the new word
	 */
	public WordPlace(String oldWord, String newWord) {
		this.oldWord = oldWord;
		
		this.newWord = newWord;
	}

	/**
	 * Gets the old word.
	 *
	 * @return the old word
	 */
	public String getOldWord() {
		return oldWord;
	}

	/**
	 * Sets the old word.
	 *
	 * @param oldWord the new old word
	 */
	public void setOldWord(String oldWord) {
		this.oldWord = oldWord;
	}

	/**
	 * Gets the new word.
	 *
	 * @return the new word
	 */
	public String getNewWord() {
		return newWord;
	}

	/**
	 * Sets the new word.
	 *
	 * @param newWord the new new word
	 */
	public void setNewWord(String newWord) {
		this.newWord = newWord;
	}
}
