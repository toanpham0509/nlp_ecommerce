package address;

import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class Street.
 */
public class Street {
	
	/** The streets. */
	private String[] streetPlaint = { 
			"giai phong", 
			
			"truong dinh", "truog dih", "truog dinh", "truong dih",  
			
			"kim dong",
			
			"ta quang buu",
			
			"ngoc hoi", 
			
			"dai tu", 
			
			"dai co viet", 
			
			"xa dan",
			
			"tran khat chan",
			
			"kim nguu", 
			
			"dai la", 
			
			"minh khai",
			
			"mai dong", 
			
			"pho vong",
			
			"bach mai", "pho hue", "trang tien", "truong trinh", "tay son",
			"lang", "nguyen trai", "xuan thuy", "cai giay", "dinh tien hoang",
			"tran thai tong", "vang dai 3", "nguyen huu tho", "ly thuong kiet"
	};
	
	private ArrayList<String> streetCode = new ArrayList<String>();

	/**
	 * Instantiates a new street.
	 */
	public Street() {
		AddressFilter addressFilter = new AddressFilter();
		for(int i = 0; i < this.streetPlaint.length; i++) {
			this.streetCode.addAll(addressFilter.convertStringFilter(this.streetPlaint[i]));
		}
	}
	
	/**
	 * Gets the streets.
	 *
	 * @return the streets
	 */
	public String[] getStreets() {
//		return this.streetPlaint;
		
		String[] stockArr = new String[streetCode.size()];
		return (String[]) streetCode.toArray(stockArr);
	}

	/**
	 * Sets the streets.
	 *
	 * @param streets the new streets
	 */
	public void setStreets(String[] streets) {
		this.streetPlaint = streets;
	}

}
