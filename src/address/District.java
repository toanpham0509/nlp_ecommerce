package address;

// TODO: Auto-generated Javadoc
/**
 * The Class District.
 */
public class District {

	/** The districts. */
	private String[] districts = { "ba dinh", "bac tu lien", "cau giay",
			"dong da", "ha dong", "hai ba trung", "hoam kiem", "hoang mai",
			"long bien", "nam tu liem", "tay ho", "thanh xuan", "son tay",
			"chuong my", "dan phuong", "dong anh", "gia lam", "hoai duc",
			"me linh", "thanh tri", "thanh oai", "thach that", "thuong tin",
			"ung hoa" };

	/**
	 * Gets the districts.
	 * 
	 * @return the districts
	 */
	public String[] getDistricts() {
		return districts;
	}

	/**
	 * Sets the districts.
	 * 
	 * @param districts
	 *            the new districts
	 */
	public void setDistricts(String[] districts) {
		this.districts = districts;
	}
}