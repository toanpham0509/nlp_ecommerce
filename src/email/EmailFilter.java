package email;

import java.util.regex.Pattern;

import address.AddressFilter;

// TODO: Auto-generated Javadoc
/**
 * The Class Email.
 */
public class EmailFilter {
	
	/** The input. */
	private String input;
	
	/**
	 * Instantiates a new email filter.
	 *
	 * @param input the input
	 */
	public EmailFilter(String input) {
		this.input = input;
	}
	
	/**
	 * Filter.
	 *
	 * @return the string
	 */
	@SuppressWarnings("static-access")
	public String filter() {
		String words[] = this.input.split(" ");

		int countWord = words.length;
		for (int i = 0; i < countWord; i++) {
			if (this.isVaidEmail(words[i])) {
				words[i] = "***";
			}
		}
			
		return new String().join(" ", words);
	}
	
	/**
	 * Checks if is vaid email.
	 *
	 * @param input the input
	 * @return true, if is vaid email
	 */
	private boolean isVaidEmail(String input) {
		Pattern pt = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		if(pt.matcher(input).find()) {
			return true;
		}
		return false;
	}
}