package phone;
import java.util.ArrayList;


public class Test {
	public static void main(String[] args) {
		String ss=CheckNumberPhone("tungpm 0163-567-2888-(+84)016356728888 HUGF    HDU 0972781395  HG-34 43H 43");
		System.out.println("Result: "+ ss);
	}
	public static String CheckNumberPhone(String input){
		String result=input.trim().replaceAll("-", "");
		ArrayList<String> list= InitListPhone();
		for(int i= list.size(); i>0; i--){
			try{
				int index=result.indexOf(list.get(i));
				if(index>=0){
					if(isNumber(result.substring(index+list.get(i).length(),index+list.get(i).length()+7))){
						result=result.replace(result.substring(index,index+list.get(i).length()+7), "...");
					}
				}
			}catch(Exception err){
				System.out.println(err.getMessage());
			}
		}

		return result;
	}
	public static boolean isNumber(String text){
	    if(text != null || !text.equals("")) {
	        char[] characters = text.toCharArray();
	        for (int i = 0; i < text.length(); i++) {
	            if (characters[i] < 48 || characters[i] > 57)
	                return false;
	        }
	    }
	    return true;
	}
	
	public static ArrayList<String> InitListPhone(){
		ArrayList<String> list= new ArrayList<String>();
		list.add("098");
		list.add("+8498");
		list.add("+84098");
		list.add("(+84)98");
		list.add("(+84)098");

		list.add("097");
		list.add("+8497");
		list.add("+84097");
		list.add("(+84)97");
		list.add("(+84)097");

		list.add("096");
		list.add("+8496");
		list.add("+84096");
		list.add("(+84)96");
		list.add("(+84)096");

		list.add("0169");
		list.add("+84169");
		list.add("+840169");
		list.add("(+84)169");
		list.add("(+84)0169");

		list.add("0168");
		list.add("+84168");
		list.add("+840168");
		list.add("(+84)168");
		list.add("(+84)0168");

		list.add("0167");
		list.add("+84167");
		list.add("+840167");
		list.add("(+84)167");
		list.add("(+84)0167");

		list.add("0167");
		list.add("+84167");
		list.add("+840167");
		list.add("(+84)167");
		list.add("(+84)0167");

		list.add("0166");
		list.add("+84166");
		list.add("+840166");
		list.add("(+84)166");
		list.add("(+84)0166");

		list.add("01675");
		list.add("+84165");
		list.add("+840165");
		list.add("(+84)165");
		list.add("(+84)0165");

		list.add("0164");
		list.add("+84164");
		list.add("+840164");
		list.add("(+84)164");
		list.add("(+84)0164");

		list.add("0163");
		list.add("+84163");
		list.add("+840163");
		list.add("(+84)163");
		list.add("(+84)0163");

		list.add("0162");
		list.add("+84162");
		list.add("+840162");
		list.add("(+84)162");
		list.add("(+84)0162");

		// VINAPHONE
		list.add("091");
		list.add("+8491");
		list.add("+84091");
		list.add("(+84)91");
		list.add("(+84)091");

		list.add("094");
		list.add("+8494");
		list.add("+84094");
		list.add("(+84)94");
		list.add("(+84)094");

		list.add("0123");
		list.add("+84123");
		list.add("+840123");
		list.add("(+84)123");
		list.add("(+84)0123"); 

		list.add("0124");
		list.add("+84124");
		list.add("+840124");
		list.add("(+84)124");
		list.add("(+84)0124"); 

		list.add("0125");
		list.add("+84125");
		list.add("+840125");
		list.add("(+84)125");
		list.add("(+84)0125"); 

		list.add("0127");
		list.add("+84127");
		list.add("+840127");
		list.add("(+84)127");
		list.add("(+84)0127"); 

		list.add("0129");
		list.add("+84129");
		list.add("+840129");
		list.add("(+84)129");
		list.add("(+84)0129"); 

		list.add("088");
		list.add("+8488");
		list.add("+84088");
		list.add("(+84)88");
		list.add("(+84)088"); 

		// MOBIFONE
		list.add("093");
		list.add("+8493");
		list.add("+84093");
		list.add("(+84)93");
		list.add("(+84)093");

		list.add("090");
		list.add("+8490");
		list.add("+84090");
		list.add("(+84)90");
		list.add("(+84)090");

		list.add("0120");
		list.add("+84120");
		list.add("+840120");
		list.add("(+84)120");
		list.add("(+84)0120"); 

		list.add("0121");
		list.add("+84121");
		list.add("+840121");
		list.add("(+84)121");
		list.add("(+84)0121");

		list.add("0122");
		list.add("+84122");
		list.add("+840122");
		list.add("(+84)122");
		list.add("(+84)0122");

		list.add("0126");
		list.add("+84126");
		list.add("+840126");
		list.add("(+84)126");
		list.add("(+84)0126");

		list.add("0128");
		list.add("+84128");
		list.add("+840128");
		list.add("(+84)128");
		list.add("(+84)0128");

		return list;
	}

}