/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phone;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc
/**
 * The Class check2.
 *
 * @author thuyetpham94
 */
public class check2 {

    /** The test. */
    public String test = "296,254,204,209,291,222,275,256,274,271,252,290,292,206,236,262,261,215,251,277,269,219"
            + "226,239,220,225,293,218,221,258,297,260,213,263,205,214,272,228,238,229,259,210,257,232,235,255,203,233,299"
            + "212,276,227,208,237,234,273,294,207,270,211,216,120,121,122,126,128,123,125,127,129,164,165,166,167,168,169,199";
    
    /** The arr. */
    String[] arr = test.split(",");

    /**
     * Checkmayban.
     *
     * @param s the s
     * @return true, if successful
     */
    public boolean checkmayban(String s) {
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(s)) {
                index = 1;
            }
        }
        if (index == 1) {
            return true;
        } else {
            return false;
        }
    }

    /** The didong. */
    public String didong = "90,93,91,94,97,98,96,95,92";
    
    /** The arr 1. */
    String[] arr1 = didong.split(",");

    /**
     * Checkdidong.
     *
     * @param s the s
     * @return true, if successful
     */
    public boolean checkdidong(String s) {
        int index = 0;
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i].equals(s)) {
                index = 1;
            }
        }
        if (index == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checkdauso.
     *
     * @param s the s
     * @return true, if successful
     */
    public boolean checkdauso(String s) {
        String test = "84";
        if (s.equals(test)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checkso.
     *
     * @param s the s
     * @return true, if successful
     */
    public boolean checkso(String s) {
        Pattern pattern = Pattern.compile("^[0-9]*$");
        Matcher matcher = pattern.matcher(s);
        if (matcher.matches()) {
            if (s.length() == 7) {
                return true;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * Kiemtra.
     *
     * @param sdt the sdt
     * @return true, if successful
     */
    public boolean kiemtra(String sdt) {
        if(sdt.charAt(0) == '+'){
            if(sdt.length() >=3 && checkdauso(sdt.substring(1,3))){
                sdt = sdt.substring(3,sdt.length());
            }else{
                sdt = sdt.substring(1,sdt.length());
            }
        }else{
            if(sdt.length() >=3 && checkdauso(sdt.substring(0,2))){
                sdt = sdt.substring(2,sdt.length());
            }else{
                sdt = sdt.substring(0,sdt.length());
            }
        }
        if (String.valueOf(sdt.charAt(0)).equals("0")) {
            if (sdt.length() >= 10) {
                if (checkmayban(sdt.substring(1, 4))) {
                    sdt = sdt.substring(4, 11);
                    if (checkso(sdt)) {
                        return true;
                    } else {
                        return false;
                    }
                } else if (checkdidong(sdt.substring(1, 3))) {
                    sdt = sdt.substring(3, 10);
                    if (checkso(sdt)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }else if(!String.valueOf(sdt.charAt(0)).equals("0")){            
            if (sdt.length() >= 9) {
                if (checkmayban(sdt.substring(0, 3))) {
                    sdt = sdt.substring(3, 10);
                    if (checkso(sdt)) {
                        return true;
                    } else {
                        return false;
                    }
                } else if (checkdidong(sdt.substring(0, 2))) {
                    sdt = sdt.substring(2, 9);
                    if (checkso(sdt)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }        
        else if (sdt.length() >= 10 && sdt.length() <= 13) {
            if (checkmayban(sdt.substring(0, 3))) {
                sdt = sdt.substring(3, 10);
                if (checkso(sdt)) {
                    return true;
                } else {
                    return false;
                }
            } else if (checkdidong(sdt.substring(0, 2))) {
                sdt = sdt.substring(2, 9);
                if (checkso(sdt)) {
                    return true;
                } else {
                    return false;
                }
            }else{
                return false;
            }
        } else {
            return false;
        }
//        return true;
    }

    /**
     * Check world.
     *
     * @param sdt the sdt
     */
    public void checkWorld(String sdt) {
        if (sdt.charAt(0) == '+') {
            if (sdt.length() >= 3) {
                if (checkdauso(sdt.substring(1, 3))) {
                    sdt = sdt.substring(3, sdt.length());
                    kiemtra(sdt);
                } else {

                }
            }
        } else if (sdt.charAt(0) == '8') {
            if (sdt.length() >= 2) {
                if (checkdauso(sdt.substring(0, 2))) {
                    sdt = sdt.substring(2, sdt.length());
                    kiemtra(sdt);
                } else {

                }
            }
        } else {
            kiemtra(sdt);
        }
    }

    /**
     * Gets the phone.
     *
     * @param sdt the sdt
     * @return the phone
     */
    public ArrayList getPhone(String sdt) {
        String[] arr = sdt.split("");
        ArrayList list = new ArrayList();
        String number = "";
        String b = "";
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals("+") || arr[i].equals("8")) {
                if (b.length() != 0) {
                    list.add(b);
                    b = "";
                }

                number += arr[i];
            } else if (checkNumber(arr[i]) || arr[i].equals("-") || arr[i].equals("_")) {
                if (b.length() != 0) {
                    list.add(b);
                    b = "";
                }                

                number += arr[i];
            } else if (!checkNumber(arr[i]) || removeSpace(number).length() > 11) {
                b += arr[i];
                if (number.length() != 0) {
                    list.add(number);
                    number = "";
                }
            }
//            
            if(i==arr.length -1){
                if (number.length() != 0) {
                    list.add(number);
                    number = "";
                }
                if (b.length() != 0) {
                    list.add(b);
                    b = "";
                }
            }           
        }
//        for(int j=0;j<list.size();j++){
//            System.out.println("list:"+list.get(j));
//        }
        return list;
    }

    /**
     * Check number.
     *
     * @param s the s
     * @return true, if successful
     */
    public boolean checkNumber(String s) {
        String check = "0123456789";
        if (check.contains(s)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Removes the space.
     *
     * @param s the s
     * @return the string
     */
    public String removeSpace(String s){
        String result = "";
        char[] str  = s.toCharArray();
        
        for(int i=0;i<str.length ;i++){
            if(str[i] == ' ' || str[i] == '-' || str[i] == '_'){
                
            }else{
                result += str[i];
            }
        }
        return result;
    } 
}
