/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phone;

import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class PhoneFilter.
 *
 * @author thuyetpham94
 */
public class PhoneFilter {
    
    /**
     * Filter.
     *
     * @param phone the phone
     * @return the string
     */
    public static String filter(String phone){
        check2 ck2 = new check2();                 
        String trim = phone.trim();
        @SuppressWarnings("unchecked")
		ArrayList<String> arrayList = ck2.getPhone(trim);
        for(int i=0;i<arrayList.size() ;i++){
            if(arrayList.get(i).equals(" ")){
                arrayList.set(i, " ");
            }else if(ck2.kiemtra(ck2.removeSpace(String.valueOf(arrayList.get(i))))){
                arrayList.set(i, "***");
            }
        }
        String result = "";
        for(int j=0;j<arrayList.size() ;j++){
            result += String.valueOf(arrayList.get(j));
            
        }
        return result;
    }
}
